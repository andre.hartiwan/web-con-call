<?php

class Slider extends Eloquent{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sliders';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    /*Automatic delete Image*/
    public static function boot() {
        parent::boot();
        static::deleting(function($slider)
        {
            try {
                if($slider->image) {
                    $image = $slider->image;
                    $deleted_path = Config::get('image_path.slider').DIRECTORY_SEPARATOR.$image;
                    if(file_exists($deleted_path)) {
                        File::delete($deleted_path);
                    }
                }
            } catch (Exception $e) {
                return false;
            }
            return true;
        });
    }

}

<?php

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Product extends Eloquent implements SluggableInterface {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    use SluggableTrait;
    protected $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'slug',
    );
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

}

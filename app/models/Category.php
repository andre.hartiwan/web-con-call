<?php

class Category extends Eloquent{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */


    public static function hasChild($id)
    {
        $parent = Category::where('parent_id', $id)->first();
        if($parent)
            return true;
        else
            return false;
    }


    /*Automatic delete Image*/
    public static function boot() {
        parent::boot();
        static::deleting(function($category)
        {
            try {
                if($category->image) {
                    $image = $category->image;
                    $deleted_path = Config::get('image_path.category').DIRECTORY_SEPARATOR.$image;
                    if(file_exists($deleted_path)) {
                        File::delete($deleted_path);
                    }
                }
            } catch (Exception $e) {
                return false;
            }
            return true;
        });
    }

}

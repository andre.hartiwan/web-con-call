<?php

return [

    /*
     *  App's image category path
     */
    'category' => public_path() . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR. 'category'.DIRECTORY_SEPARATOR,
    'slider' => public_path() . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR. 'slider'.DIRECTORY_SEPARATOR,
    'product' => public_path() . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR. 'product'.DIRECTORY_SEPARATOR

];

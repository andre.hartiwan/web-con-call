<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        Sentry::getUserProvider()->create(array(
            'email'    => 'admin@ecommerce.com',
            'password' => '12345678',
            'first_name' => 'Admin',
            'last_name' => 'Ecommerce',
            'activated' => '1'
        ));
    }
}

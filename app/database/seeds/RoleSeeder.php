<?php

    class RoleSeeder extends Seeder
    {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $super_admin = new Role;
            $super_admin->name = 'Super Admin';
            $super_admin->save();

            $room_admin = new Role;
            $room_admin->name = 'Room Admin';
            $room_admin->save();

            $attendance = new Role;
            $attendance->name = 'Attendance';
            $attendance->save(); 

            $super_admin_perm = new Permission;
            $super_admin_perm->name = 'access_all';
            $super_admin_perm->display_name = 'All Access Permission';
            $super_admin_perm->save();

            $manage_room_perm = new Permission;
            $manage_room_perm->name = 'manage_room';
            $manage_room_perm->display_name = 'Manage conference room';
            $manage_room_perm->save();

            $manage_attendance_perm = new Permission;
            $manage_attendance_perm->name = 'manage_attendance';
            $manage_attendance_perm->display_name = 'Manage conference room attendance';
            $manage_attendance_perm->save();

            $manage_contact_perm = new Permission;
            $manage_contact_perm->name = 'manage_contact';
            $manage_contact_perm->display_name = 'Manage attendance contact';
            $manage_contact_perm->save();

            $manage_schedule_perm = new Permission;
            $manage_schedule_perm->name = 'manage_schedule';
            $manage_schedule_perm->display_name = 'Manage conference schedule';
            $manage_schedule_perm->save();

            $manage_notification = new Permission;
            $manage_notification->name = 'manage_notification';
            $manage_notification->display_name = 'Manage notification to attendnce';
            $manage_notification->save();

            $manage_call = new Permission;
            $manage_call->name = 'manage_call';
            $manage_call->display_name = 'Manage call';
            $manage_call->save();

            $super_admin->perms()->sync(array($super_admin_perm->id));
            $room_admin->perms()->sync(array($manage_room_perm->id, $manage_attendance_perm->id, $manage_contact_perm->id, $manage_schedule_perm->id, $manage_notification->id, $manage_call->id));
            $attendance->perms()->sync(array($manage_call->id));

            $user = Sentry::getUserProvider()->create([
                'email' => 'admin@webcon.com',
                'password' => '12345678',
                'activated' => '1',
                'first_name' => 'Admin',
                'last_name' => 'Webcon',
                'role' => 1
            ]);

            $user_entity = User::where('id','=',$user->id)->first();
            $user_entity->roles()->attach($super_admin->id);
        }

    }

<?php

class AdminSeeder extends Seeder {

    public function run()
    {
        $user = Sentry::getUserProvider()->create([
            'email' => 'admin@webcon.com',
            'password' => '12345678',
            'activated' => '1',
            'first_name' => 'Admin',
            'last_name' => 'Webcon',
            'role' => 1
        ]);

        $user_entity = User::where('id','=',$user->id)->first();
        $user_entity->roles()->attach(1);
    }
}

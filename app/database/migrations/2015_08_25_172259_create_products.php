<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function($table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->string('model');
            $table->string('description');
            $table->string('highlight');
            $table->string('image');
            $table->integer('viewed');
            $table->integer('status');
            $table->integer('quantity');
            $table->decimal('weight');
            $table->decimal('price');
            $table->decimal('discount');
            $table->string('size', 3);
            $table->integer('category_id');
            $table->string('size_details');
            $table->string('return_policy');
            $table->string('delivery_info');
            $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}

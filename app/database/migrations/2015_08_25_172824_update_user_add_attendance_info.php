<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserAddAttendanceInfo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table) {
            $table->integer('role')->after('last_name');
            $table->string('mobile_phone')->after('role');
            $table->integer('is_new')->after('mobile_phone');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($table) {
            $table->dropColumn('role');
            $table->dropColumn('mobile_phone');
            $table->dropColumn('is_new');
        });
	}

}

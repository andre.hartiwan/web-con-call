<?php
	
	class UserEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
                $user = Sentry::getUserProvider()->findById($data['user_id']);
                $user->first_name = $data['first_name'];
                $user->last_name = $data['last_name'];
                $user->email = $data['email'];
                $user->mobile_phone = $data['mobile_phone'];
                $user->role = $data['role'];

                if($data['password'])
                {
                    $user->password = $data['password'];
                }

                AssignedRoleManager::updateRole($data['user_id'], $data['role']);

                $user->save();

               	return TRUE;
			}
			catch (Exception $e)
            {
                $this->error = $e->getMessage();
                return FALSE;
            }
		}
	}
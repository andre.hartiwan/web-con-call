<?php
	
	class UserAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
                $password = Helper::generatePassword(8);

                $user_id = UserManager::save($data['first_name'], $data['last_name'], $data['email'], $password, $data['mobile_phone'], $data['role']);
                UserManager::attachRole($user_id, $data['role']);

                $user_data = array(
                    'name' => $data['first_name'],
                    'email' => $data['email'],
                    'password' => $password,
                    'role' => RoleManager::getRoleName($data['role']),
                );

                Mail::send('emails.user.new_user_template', $user_data, function($message) use ($user_data) {
                    $message->to($user_data['email'], $user_data['name'])
                            ->subject('New User Notification');
                });

               	return TRUE;
			}
			catch (Exception $e)
            {
                $this->error = $e->getMessage();
                return FALSE;
            }
		}
	}
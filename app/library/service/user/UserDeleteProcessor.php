<?php
	
	class UserDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
                UserManager::delete($data['id']);
               	return TRUE;
			}
			catch (Exception $e)
            {
                $this->error = $e->getMessage();
                return FALSE;
            }
		}
	}
<?php
	
	class AuthSigninProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
                $credentials = array(
	                'email' => $data['email'],
	                'password' => $data['password']
                );

                if($data['remember_me'] == '1')
                {
                    $user = Sentry::authenticateAndRemember($credentials);
                }
                else
                {
                    $user = Sentry::authenticate($credentials, TRUE);
                }
                
                $this->output = $user->is_new;

               	return TRUE;
			}
			catch (\Cartalyst\Sentry\Users\LoginRequiredException $e)
            {
                $this->error = 'Login field is required.';
                return FALSE;
            }
            catch (\Cartalyst\Sentry\Users\PasswordRequiredException $e)
            {
                $this->error = 'Password field is required.';
                return FALSE;
            }
            catch (\Cartalyst\Sentry\Users\WrongPasswordException $e)
            {
                $this->error = 'Wrong password, try again.';
                return FALSE;
            }
            catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                $this->error = 'User was not found.';
                return FALSE;
            }
            catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e)
            {
                $this->error = 'User is not activated.';
                return FALSE;
            }
            catch (\Cartalyst\Sentry\Throttling\UserSuspendedException $e)
            {
                $this->error = 'User is suspended.';
                return FALSE;
            }
            catch (\Cartalyst\Sentry\Throttling\UserBannedException $e)
            {
                $this->error = 'User is banned.';
                return FALSE;
            }
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}
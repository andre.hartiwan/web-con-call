<?php
	
	class AuthUpdatePwdProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$user = Sentry::getUserProvider()->findById($data['user_id']);
				$user->password = $data['password']; 
				$user->is_new = '0';
				$user->save();

               	return TRUE;
			}
			catch (Exception $e)
            {
                $this->error = $e->getMessage();
                return FALSE;
            }
		}
	}
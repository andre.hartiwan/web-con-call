<?php
	
	class Message
	{
		public $message_code;
		public $message;
		public $data;

		public function __construct($message_code, $data)
		{
			$this->message_code = (int) $message_code;
			$this->message = $this->getMessage($this->message_code);
			$this->data = $data;
		}

		/**
         * Return message string by its code
         *
         * @param int $code 
         *
         * @return string
         */
		public function getMessage($code)
		{
			switch($code)
			{
				case 0:
					return 'Success';
					break;
					
				case 1:
					return "User successfully signed in";
					break;

				case 2:
					return "User successfully signed out.";
					break;

				case 3:
					return "User successfully signed up.";
					break;

				case 4:
					return "User profile successfully updated.";
					break;

				case 5:
					return "Password successfully updated.";
					break;

				case 6:
					return "User status is successfully updated.";
					break;

                case 7:
                    return "Data is successfully updated.";
                    break;

                case 8:
                    return "Schedule is successfully updated.";
                    break;

                case 9:
                    return "Schedule is successfully deleted.";
                    break;

                case 1000:
                    return "System error.";
                    break;

				case 2000:
					return 'Insufficient privilege';
					break;

				case 2001:
					return 'Invalid token';
					break;

				case 2002:
					return 'You are not logged in yet';
					break;

				case 2003:
					return 'Token mismatch';
					break;

				case 2004:
					return 'Wrong Password';
					break;

				case 2005:
					return 'Invalid username or password';
					break;

				case 2006:
					return 'User not active';
					break;

				case 2007:
					return 'User is suspended';
					break;

				case 2008:
					return 'User is banned';
					break;

				case 2009:
					return 'User exist';
					break;

				case 2010:
					return 'Unknown error occured';
					break;

				case 2011:
					return 'User not found';
					break;

				case 2012:
					return 'Password mismatch';
					break;

				case 2013:
					return 'User successfully joined';
					break;

                case 2014:
                    return 'User successfully unjoined';
                    break;

				case 9100:
					return 'Bad syntax. Plase provide more parameters';
					break;

				case 9101:
					return 'Bad syntax. Unknown parameter';
					break;

				case 9102:
					return 'Bad syntax. Url format not recognized';
					break;

				case 9103:
					return 'Feed is successfully deleted';
					break;

                case 9104:
                    return 'Notification status is succesfully updated';
                    break;

                case 9105:
                    return 'Data not found';
                    break;

                case 9106:
                    return 'Duplicate sport is not permitted';
                    break;

                case 9107:
                    return 'Data validation error';
                    break;

                case 9108:
                    return 'User is successfully added as admin';
                    break;

                case 9109:
                    return 'Permissions is successfully added';
                    break;

                case 9110:
                    return 'Participant is successfully added.';
                    break;

                case 9111:
                    return 'Admin is successfully deleted.';
                    break;

                case 9112:
                    return 'Permissions is successfully removed';
                    break;
			}
		}

		/**
         * Return a formatted message
         *
         * @param object $message 
         *
         * @return array
         */
        public function formatMessage($message)
        {
        	return array(
        		'code' => $this->message_code,
			    'message' => $this->message,

        	);
        }

        /**
         * Return a success message (message with code 0)
         *
         *
         * @return array
         */
        public static function getSuccessMessage()
		{
			$message = new Message(0, NULL);
			return $message->getMessage(0);
		}
	}
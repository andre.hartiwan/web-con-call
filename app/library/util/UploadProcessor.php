<?php
	
	class UploadProcessor
	{
		private $file_object;

		private $destinantion;

		private $filename;

		private $upload_status;

		private $file_type;

		public function __construct($file_object, $destination)
		{
			$this->file_object = $file_object;
			$this->destination = $destination;
		}

		public function upload()
		{
			$this->filename = $this->file_object->getClientOriginalName();
			$this->file_object->move($this->destination, $this->filename);
		}

		public function getFilename()
		{
			return $this->filename;
		}

		public function getFilePath()
		{
			return $this->destination . $this->filename;
		}

		public function getFileType()
		{
			return $this->file_type;
		}
	}
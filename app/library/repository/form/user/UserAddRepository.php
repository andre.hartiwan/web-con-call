<?php
	
	class UserAddRepository extends BaseRepository
	{
		private $first_name;
		private $last_name;
		private $email;
		private $mobile_phone;
		private $role;

		public function getInput()
		{
			$this->first_name = Input::get('first_name');
			$this->last_name = Input::get('last_name');
			$this->email = Input::get('email');
			$this->mobile_phone = Input::get('mobile_phone');
			$this->role = Input::get('role');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'first_name' => $this->first_name,
	            'last_name' => $this->last_name,
	            'email' => $this->email,
	            'mobile_phone' => $this->mobile_phone,
	            'role' => $this->role
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'first_name' => 'required',
	            'email' => 'required|email|unique:users,email',
	            'mobile_phone' => 'required',
	            'role' => 'required'
			);
		}
	}
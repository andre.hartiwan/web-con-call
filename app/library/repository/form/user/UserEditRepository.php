<?php
	
	class UserEditRepository extends BaseRepository
	{
		private $user_id;
		private $first_name;
		private $last_name;
		private $email;
		private $mobile_phone;
		private $password;
		private $password_confirm;
		private $role;

		public function getInput()
		{
			$this->user_id = Input::get('user_id');
			$this->first_name = Input::get('first_name');
			$this->last_name = Input::get('last_name');
			$this->email = Input::get('email');
			$this->mobile_phone = Input::get('mobile_phone');
			$this->password = Input::get('password');
			$this->password_confirm = Input::get('password_confirm');
			$this->role = Input::get('role');
		}

		public function setValidationData()
		{
			$this->data = array(
				'user_id' => $this->user_id,
	            'first_name' => $this->first_name,
	            'last_name' => $this->last_name,
	            'email' => $this->email,
	            'mobile_phone' => $this->mobile_phone,
	            'password' => $this->password,
	            'password_confirm' => $this->password_confirm,
	            'role' => $this->role
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'user_id' => 'required',
				'first_name' => 'required',
	            'email' => 'required|email|unique:users,email,' . $this->user_id,
	            'mobile_phone' => 'required',
	            'password' => 'same:password_confirm',
	            'password_confirm' => 'same:password',
	            'role' => 'required'
			);
		}
	}
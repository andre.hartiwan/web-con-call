<?php
	
	class AuthSigninRepository extends BaseRepository
	{
		private $email;
		private $password;
		private $remember_me;

		public function getInput()
		{
			$this->email = Input::get('email');
			$this->password = Input::get('password');
			$this->remember_me = Input::get('remember_me');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'email' => $this->email,
	            'password' => $this->password,
	            'remember_me' => $this->remember_me
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'email' => 'required|email',
	            'password' => 'required'
			);
		}
	}
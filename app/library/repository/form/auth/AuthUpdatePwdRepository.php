<?php
	
	class AuthUpdatePwdRepository extends BaseRepository
	{
		private $user_id;
		private $password;
		private $confirm_password;

		public function getInput()
		{
			$this->user_id = Input::get('user_id');
			$this->password = Input::get('password');
			$this->confirm_password = Input::get('confirm_password');
		}

		public function setValidationData()
		{
			$this->data = array(
				'user_id' => $this->user_id,
	            'password' => $this->password,
	            'confirm_password' => $this->confirm_password
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'user_id' => 'required',
	            'password' => 'required',
	            'confirm_password' => 'required|same:password'
			);
		}
	}
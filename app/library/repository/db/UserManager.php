<?php

	class UserManager
	{
		public static function save($first_name, $last_name, $email, $password, $mobile_phone, $role)
		{
			$user = Sentry::getUserProvider()->create([
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'password' => $password,
                'mobile_phone' => $mobile_phone,
                'activated' => true,
                'role' => $role,
                'is_new' => true
            ]);

            return $user->id;
		}

		public static function get($user_id)
		{
			$user = User::find($user_id);
			return $user;
		}

		public static function attachRole($user_id, $role)
		{
			$user_entity = User::where('id','=',$user_id)->first();
	        $user_entity->roles()->attach($role);
		}

		public static function generateDataTables()
		{
			$users = new User();

            $user = Sentry::getUser();

            $query = $users->select('id', 'first_name', 'last_name', 'email', 'role', 'last_login', 'activated')->where('id','!=',$user->id)->orderBy('created_at','DESC')->get();
            
            $data = Datatable::collection($query);
            
            return $data
                ->addColumn('first_name', function($model) {
                    return $model->first_name;
                })
                ->addColumn('last_name', function($model) {
                    return $model->last_name;
                })
                ->addColumn('email', function($model) {
                    return $model->email;
                })
                ->addColumn('role', function($model) {
                    return RoleManager::getRoleName($model->role);
                })
                ->addColumn('last_login', function($model) {
                    return $model->last_login ? $model->last_login : 'Not logged in yet';
                })
                ->addColumn('status', function($model) {
                    $activated = ($model->activated != 1) ? '<label class="label label-warning"> Deactive </label>' : '<label class="label label-success"> Active </label>' ;
                    return $activated;
                })
                ->addColumn('action', function($model) {
                    $action  = '';
                    $action .= '<a href="'.URL::to("admin/user/edit?id=".$model->id).'" class="btn btn-success btn-flat" title="Edit User" content-id ="'.$model->id.'" message=""><i class="fa fa-pencil"></i></a>';
                    // $action .= '<a href="javascript:void(0)" class="btn btn-primary btn-flat" title="View User" content-id ="'.$model->id.'" message="" onclick="getDetail(this)"><i class="fa fa-search-plus"></i></a>';
                    $action .=  '<button href="" class="btn btn-danger btn-flat delete" title="Delete User" data-id="' . $model->id . '"><i class="fa fa-eraser"></i></button>';
                    return $action;
                })
                ->searchColumns('first_name', 'last_name', 'email')
                ->orderColumns('first_name', 'last_name', 'email')
                ->make();
		}

        public static function delete($user_id)
        {
            User::where('id',$user_id)->delete();
        }
	}
<?php

	class RoleManager
	{
		public static function getRoleName($role_id)
		{
			$role = Role::find($role_id);
			return $role->name;
		}

		public static function getRoleIdByName($role_name)
		{
			$role = Role::select('id')->where('name',$role_name)->first();
			return $role->id;
		}
	}
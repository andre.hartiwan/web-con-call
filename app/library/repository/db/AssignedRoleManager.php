<?php

	class AssignedRoleManager
	{
		public static function updateRole($user_id, $role)
		{
			DB::table('assigned_roles')->where('user_id',$user_id)->update(array(
				'role_id' => $role
			));
		}

		public static function deleteRole($user_id)
		{
			DB::table('assigned_roles')->where('user_id',$user_id)->delete();
		}
	}
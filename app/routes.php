<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Routes Frontend*/
Route::get('/',array(
    'as' => 'auth.signin',
    'uses' => 'AuthController@getSignin'
));



Route::group(array('prefix' => 'auth'), function()
{
    //Route::get('/', '   @getLogin');

    Route::get('/signin',array(
        'as' => 'auth.signin',
        'uses' => 'AuthController@getSignin'
    ));

    Route::post('/signin', array(
        'as' => 'auth.signin.do',
        'uses' => 'AuthController@postSignin'
    ));

    Route::get('/signout',array(
        'as' => 'auth.signout',
        'uses' => 'AuthController@getSignout'
    ));

    Route::get('/do-forgot-password',array(
        'as' => 'auth.forgot.password',
        'uses' => 'AuthController@postForgotPassword'
    ));

    Route::post('request-new-password', array(
        'as' => 'auth.new.password',
        'uses' => 'AuthController@postRequestPassword'
    ));

    Route::get('reset-password/{encripted}', array(
        'as' => 'auth.reset.password',
        'uses' => 'AuthController@getResetPassword'
    ));

    Route::post('post-reset-password', array(
        'as' => 'auth.reset.password',
        'uses' => 'AuthController@postUpdatePassword'
    ));

    Route::post('update-password', array(
        'as' => 'auth.update.password',
        'uses' => 'AuthController@postUpdatePassword'
    ));

});

Route::group(array('before' => 'authorized', 'prefix' => 'admin'), function() {

    /*Dashboard Route*/
    Route::group(array('prefix' => 'dashboard'), function() {

        Route::get('/', array(
            'as' => 'dashboard.index',
            'uses' => 'DashboardController@getIndex'
        ));

    });

    /*User Routes*/
    Route::group(array('before' => 'super_admin', 'prefix' => 'user'), function() {

        Route::get('/', array(
            'as' => 'user.index',
            'uses' => 'UserController@getIndex'
        ));

        Route::get('/create', array(
            'as' => 'user.create.get',
            'uses' => 'UserController@getCreate'
        ));

        Route::post('/create', array(
            'as' => 'user.create.save',
            'uses' => 'UserController@postCreate'
        ));

        Route::get('/edit', array(
            'as' => 'user.edit.get',
            'uses' => 'UserController@getEdit'
        ));

        Route::post('/edit', array(
            'as' => 'user.edit.save',
            'uses' => 'UserController@postEdit'
        ));

        Route::post('/delete', array(
            'as' => 'user.delete',
            'uses' => 'UserController@postDelete'
        ));

        Route::get('api/users', array(
            'as' => 'admin.api.users',
            'uses' => 'UserController@getUserDataTables'
        ));

    });



    /*Routes Category*/
    Route::get('category', 'CategoryController@getIndex');
    Route::get('category/create', 'CategoryController@getCreate');
    Route::get('category/edit/{id}', 'CategoryController@getEdit');
    Route::post('category/save', 'CategoryController@postSave');
    Route::post('category/delete', 'CategoryController@postDelete');
    Route::get('api/categories', array(
        'as' => 'admin.api.categories',
        'uses' => 'CategoryController@getDataTables'
    ));


    /*Routes Sliders*/
    Route::get('slider', 'SliderController@getIndex');
    Route::get('slider/create', 'SliderController@getCreate');
    Route::get('slider/edit/{id}', 'SliderController@getEdit');
    Route::post('slider/save', 'SliderController@postSave');
    Route::post('slider/delete', 'SliderController@postDelete');
    Route::get('api/sliders', array(
        'as' => 'admin.api.sliders',
        'uses' => 'SliderController@getDataTables'
    ));

    /*Routes Product*/
    Route::get('product', 'ProductController@getIndex');
    Route::get('product/create', 'ProductController@getCreate');
    Route::get('product/edit/{id}', 'ProductController@getEdit');
    Route::post('product/save', 'ProductController@postSave');
    Route::post('product/delete', 'ProductController@postDelete');

    Route::get('api/products', array(
        'as' => 'admin.api.products',
        'uses' => 'ProductController@getDataTables'
    ));

    /*Route Uploader*/
    Route::post('upload/image/category', 'UploadController@postCategory');
    Route::post('upload/image/slider', 'UploadController@postSlider');
    Route::post('upload/image/product', 'UploadController@postProduct');
});
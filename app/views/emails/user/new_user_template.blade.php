<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h4>Hello {{$name}}</h4>
        <p>You've been added as {{ $role }}</p>
        <p>Here's your login detail: </p>
        <p>Email: {{$email}}</p>
        <p>Password: {{$password}}</p>
        <p>Click <a href="{{ URL::route('auth.signin') }}">here</a> to login</p>
        
    </body>
</html>
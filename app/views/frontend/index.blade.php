@extends('layouts.frontend')
@section('content')

    <!--BEGIN PAGE-->
    <div class="col-12" >
        <!--BEGIN DASHBOARD-->
        <div style="margin-left:auto;margin-right:auto;width:200px;">
            <h1 class="padding-high" style="font-size:60px; z-index:9999; background-color:rgba(0, 0, 0, 0.42);position:absolute;color:white;margin-top:240px;">BATIK WARISAN</h1>
        </div>
        <div class="col-12  " style="height:640px">
            <!--BEGIS SLIDER-->
            <div class="content-slider">
                <div id="dashboard-carousel" class="carousel slide" data-ride="carousel" >
                    <!-- Indicators -->
                    <ol class="carousel-indicators" style="left:350px !important;">
                        <li data-target="#dashboard-carousel" data-slide-to="0" class="active circle" style="width:20px;height:20px;"></li>
                        <li data-target="#dashboard-carousel" data-slide-to="1" class=" circle" style="width:20px;height:20px;"></li>
                        <li data-target="#dashboard-carousel" data-slide-to="2" class=" circle" style="width:20px;height:20px;"></li>
                        <li data-target="#dashboard-carousel" data-slide-to="3" class=" circle" style="width:20px;height:20px;"></li>
                        <li data-target="#dashboard-carousel" data-slide-to="4" class=" circle" style="width:20px;height:20px;"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="{{asset('assets/frontend/images/01.jpg')}}" class="img-responsive" alt="Berry Lace Dress" width="1280" height="600">
                        </div>
                        <div class="item">
                            <img src="{{asset('assets/frontend/images/02.jpg')}}" class="img-responsive" alt="Berry Lace Dress" width="1280" height="600">
                        </div>
                        <div class="item">
                            <img src="{{asset('assets/frontend/images/03.jpg')}}" class="img-responsive" alt="Berry Lace Dress" width="1280" height="600">
                        </div>
                        <div class="item">
                            <img src="{{asset('assets/frontend/images/04.jpg')}}" class="img-responsive" alt="Berry Lace Dress" width="1280" height="600">
                        </div>
                        <div class="item">
                            <img src="{{asset('assets/frontend/images/05.jpg')}}" class="img-responsive" alt="Berry Lace Dress" width="1280" height="600">
                        </div>
                    </div>
                </div>
            </div>
            <!--END SLIDER-->
        </div>
        <div class="col-12" style="height:300px">
            <div class="col-6 pull-left banner-hover" style="height:300px;">
                <div style="width:640px;height:300px;position:absolute;">
                    <h1 class="padding-high" style="z-index:9999;position:absolute;color:white;margin-top:70px;">JUNE</h1>
                </div>
                <img src="{{asset('assets/frontend/images/banner-new-arival.jpg')}}" class="col-12  " height="300">
            </div>
            <div class="col-6 pull-right banner-hover" style="height:300px;">
                <div style="width:640px;height:300px;position:absolute;">
                    <h1 class="padding-high" style="font-size:60px; z-index:9999;position:absolute;color:white;margin-top:70px;">JUNE</h1>
                </div>
                <img src="{{asset('assets/frontend/images/banner-dresses.jpg')}}" class="col-12  " height="300">
            </div>
        </div>
        <div class="col-12  " style="height:300px">
            <table class="col-12">
                <tr>
                    <td class="col-4 banner-hover" style="height:300px">
                        <div style="width:425px;height:300px;position:absolute;">
                            <h1 class="padding-high" style="font-size:60px; z-index:9999;position:absolute;color:white;margin-top:70px;">JUNE</h1>
                        </div>
                        <img src="{{asset('assets/frontend/images/banner-top.jpg')}}" class="col-12 banner-hover" height="300">
                    </td>
                    <td class="col-4 banner-hover" style="height:300px">
                        <div style="width:425px;height:300px;position:absolute;">
                            <h1 class="padding-high" style="font-size:60px; z-index:9999;position:absolute;color:white;margin-top:70px;">JUNE</h1>
                        </div>
                        <img src="{{asset('assets/frontend/images/banner-bottom.jpg')}}" class="col-12 banner-hover" height="300">
                    </td>
                    <td class="col-4 banner-hover" style="height:300px">
                        <div style="width:425px;height:300px;position:absolute;">
                            <h1 class="padding-high" style="font-size:60px; z-index:9999;position:absolute;color:white;margin-top:70px;">JUNE</h1>
                        </div>
                        <img src="{{asset('assets/frontend/images/banner-outer.jpg')}}" class="col-12 banner-hover" height="300">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-12" style="height:300px">
            <div class="col-12 banner-hover" style="height:300px">
                <div style="width:1280px;height:300px;position:absolute;">
                    <h1 class="padding-high" style="font-size:60px; z-index:9999;position:absolute;color:white;margin-top:70px;">JUNE</h1>
                </div>
                <img src="{{asset('assets/frontend/images/banner-megazine.jpg')}}" class="col-12" height="300">
            </div>
        </div>
        <!--END DASHBOARD-->
    </div>
    <!--END PAGE-->
<!--END CONTENT-->
@stop
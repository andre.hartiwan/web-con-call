@extends('layouts.frontend')
@section('content')
    <!--BEGIN PAGE-->
    <div class="col-12" >
        <!--BEGIN TOPS-->
        <div class="col-12  " style="height:640px">
            <img src="{{asset('assets/frontend/images//banner.jpg')}}" class="col-12" style="height:640px;" />
        </div>
        <div class="col-12">
            <table class="col-12">
                <tr>
                    <td class="col-2">
                        <img src="{{asset('assets/frontend/images/upper-left.png')}}" class="col-12" />
                    </td>
                    <td class="col-8 text-center">
                        <p style="font-size:36px;color:#58260c">TOPS</p>
                        <img src="{{asset('assets/frontend/images/under-text.png')}}" style="width:200px;margin-left:auto;margin-right:auto" />
                        <div class="col-12 text-center padding-high" style="margin-top:30px;">
                            <div class="col-md-11">
                                <select class="form-control">
                                    <option>Sample</option>
                                </select>
                            </div>
                            <div class="col-md-1">
                                <button type="submit" style="background-color:#58260c" class="btn"><i class="fa fa-search" style="color:white"></i></button>
                            </div>
                        </div>

                    </td>
                    <td class="col-2">
                        <img src="{{asset('assets/frontend/images/upper-right.png')}}" class="col-12" />
                    </td>
                </tr>
            </table>
        </div>
        <!--BEGIN PRODUCT LIST-->
        <div class="col-12  ">
            <table class="col-12">
                <tr>
                    <td class="col-2"></td>
                    <td class="col-8">
                        <!-- PRODUCT ITEM START -->
                        <div class="col-md-4" style="margin-bottom:5px !important;">
                            <div class="product-item text-center">
                                <div class="pi-img-wrapper">
                                    <img src="{{asset('assets/frontend/images/model1.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                                    <div>
                                        <a href="{{asset('assets/frontend/images/model1.jpg')}}" class="btn btn-default fancybox-fast-view">Zoom</a>
                                    </div>
                                </div>
                                <h2>Berry Lace Dress</h2>
                                <h2 style="color:#58260c">$29.00</h2>
                                <br />
                                <a href="#"  class="btn btn-default">View</a>
                            </div>
                        </div>
                        <!-- PRODUCT ITEM START -->
                        <div class="col-md-4" style="margin-bottom:5px !important;">
                            <div class="product-item text-center">
                                <div class="pi-img-wrapper">
                                    <img src="{{asset('assets/frontend/images/model1.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                                    <div>
                                        <a href="{{asset('assets/frontend/images/model1.jpg')}}" class="btn btn-default fancybox-fast-view">Zoom</a>
                                    </div>
                                </div>
                                <h2>Berry Lace Dress</h2>
                                <h2 style="color:#58260c">$29.00</h2>
                                <br />
                                <a href="#" data-toggle="tab" class="btn btn-default">View</a>
                            </div>
                        </div>

                        <!-- PRODUCT ITEM START -->
                        <div class="col-md-4" style="margin-bottom:5px !important;">
                            <div class="product-item text-center">
                                <div class="pi-img-wrapper">
                                    <img src="{{asset('assets/frontend/images/model1.jpg')}}" class="img-responsive" alt="Berry Lace Dress">
                                    <div>
                                        <a href="{{asset('assets/frontend/images/model1.jpg')}}" class="btn btn-default fancybox-fast-view">Zoom</a>
                                    </div>
                                </div>
                                <h2>Berry Lace Dress</h2>
                                <h2 style="color:#58260c">$29.00</h2>
                                <br />
                                <a href="#" data-toggle="tab" class="btn btn-default">View</a>
                            </div>
                        </div>

                    </td>
                    <td class="col-2"></td>
                </tr>
            </table>


            <!--BEGIN PAGINATION-->
            <div class="col-12 padding-high text-center">
                THIS IS MY PAGINATION
            </div>
            <!--END PAGINATION-->
        </div>
        <!--END PRODUCT LIST-->
        <!--END TOPS-->
    </div>
    <!--END PAGE-->
@stop
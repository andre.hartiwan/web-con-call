@extends('layouts.backend')
@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Slider</h3>
                <h2><a href="{{URL::to('admin/slider/create')}}" class="btn btn-primary btn-flat">Create New</a></h2>
            </div><!-- /.box-header -->
        <div class="box-body">
        <div class="sliderTable">
                {{ $tables->render() }}
            </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript" src="{{asset('assets/backend/js/modules/slider.js')}}"></script>
@stop

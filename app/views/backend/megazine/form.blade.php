@extends('layouts.backend')
@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
                <div class="alert alert-danger alert-dismissible hide errorMessages" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Error!</strong> <span class="messages"></span>
                </div>
            </div><!-- /.box-header -->
                <!-- form start -->
            <form role="form" id="" class="generalSubmitForm" id="sliderForm" action="{{URL::to('admin/slider/save')}}">
                <div class="box-body">
                    <div class="form-group">
                        <label>Slider Image </label>
                        <div class="input-group col-md-3">
                            <input type="hidden" name="image" id="image" value="">
                            <a href="javascript:void(0)" class="thumbnail link-uploader" onclick="" id="uploadLink" url="{{URL::to('admin/upload/image/slider')}}">
                                <img src="" class="img-uploader">
                            </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" class="form-control" id="title" name="title" value="@if(isset($slider)) {{ $slider->title }} @endif">
                        <input type="hidden" class="form-control" id="id" name="id" value="@if(isset($slider)) {{ $slider->id }} @endif">
                        <input type="hidden" class="form-control" id="action" name="action" value="@if(isset($slider)) edit @else add @endif">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Sort Order</label>
                        <input type="text" class="form-control" id="sort_order" name="sort_order" value="@if(isset($slider)) {{ $slider->sort_order }} @endif">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description</label>
                        <textarea class="form-control" id="description" name="description">@if(isset($slider)) {{ $slider->description }} @endif</textarea>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Submit</button>
                    <a href="{{URL::to('admin/slider')}}"><button type="button" class="btn btn-flat btn-default">Back</button></a>
                </div>
            </form>
        </div><!-- /.box -->
    </div><!--/.col (right) -->
</div>   <!-- /.row -->
<script type="text/javascript" src="{{asset('assets/backend/js/modules/upload.js')}}"></script>
@stop
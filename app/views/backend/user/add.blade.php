@extends('layouts.backend')

@section('title')
  User | Create
@stop

@section('page_title')
  User Create
@stop

@section('page_description')
  user create form
@stop

@section('breadcrumb')
    <li><a href="{{ URL::route('user.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Create User</li>
@stop

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
        <div class="box box-primary">
            
            <!-- form start -->
            <form id="add-user" method="post" data-op="add">    
                <div class="box-body">
                    <div class="first_name_wrapper form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name">
                        <label class="control-label error_first_name hide" for="inputError"><i class="fa fa-times-circle-o"></i> <span class="error_first_name_message"></span></label>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="last_name" class="form-control" id="last_name" name="last_name">
                    </div>
                    <div class="email_wrapper form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="text" class="form-control" id="email" name="email">
                        <label class="control-label error_email hide" for="inputError"><i class="fa fa-times-circle-o"></i> <span class="error_email_message"></span></label>
                    </div>
                    <div class="phone_wrapper form-group">
                        <label for="mobilePhone">Mobile Phone</label>
                        <input type="text" class="form-control" id="mobile_phone" name="mobile_phone">
                        <label class="control-label error_phone hide" for="inputError"><i class="fa fa-times-circle-o"></i> <span class="error_phone_message"></span></label>
                    </div>
                    <div class="form-group">
                        <label for="role">Role</label>
                        <select name="role" class="form-control" id="role">
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Submit</button>
                    <a href="{{ URL::route('user.index') }}" type="button" class="btn btn-flat btn-default">Cancel</a>
                </div>
            </form>
        </div><!-- /.box -->
    </div><!--/.col (right) -->
</div>   <!-- /.row -->
@stop

@section('scripts')
    <script src="{{ asset('assets/backend/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/backend/js/modules/user.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/backend/js/modules/errors/user_errors.js')}}"></script>
@stop
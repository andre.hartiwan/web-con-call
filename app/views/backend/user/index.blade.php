@extends('layouts.backend')

@section('title')
  User | Index
@stop

@section('page_title')
  User Index
@stop

@section('page_description')
  Users list
@stop

@section('breadcrumb')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">User</li>
@stop

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="col-md-2">
                    <a href="{{ URL::route('user.create.get') }}" class="btn btn-block btn-success btn-flat">Create User</a>
                </div>
            </div><!-- /.box-header -->
        <div class="box-body">
            <div class="userTable">
                {{ $tables->render() }}
            </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
@stop

@section('scripts')
    <script src="{{ asset('assets/backend/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/backend/js/modules/user.js')}}"></script>
@stop

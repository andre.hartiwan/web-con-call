@extends('layouts.backend')

@section('title')
  User | Edit
@stop

@section('page_title')
  User Edit
@stop

@section('page_description')
  user edit form
@stop

@section('breadcrumb')
    <li><a href="{{ URL::route('user.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Edit User</li>
@stop

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
        <div class="box box-primary">
            
            <!-- form start -->
            <form id="edit-user" method="post" data-op="edit">    
                <div class="box-body">
                    <div class="first_name_wrapper form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" value="{{ $user->first_name }}">
                        <label class="control-label error_first_name hide" for="inputError"><i class="fa fa-times-circle-o"></i> <span class="error_first_name_message"></span></label>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="last_name" class="form-control" id="last_name" name="last_name" value="{{ $user->last_name }}">
                    </div>
                    <div class="email_wrapper form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}">
                        <label class="control-label error_email hide" for="inputError"><i class="fa fa-times-circle-o"></i> <span class="error_email_message"></span></label>
                    </div>
                    <div class="phone_wrapper form-group">
                        <label for="mobilePhone">Mobile Phone</label>
                        <input type="text" class="form-control" id="mobile_phone" name="mobile_phone" value="{{ $user->mobile_phone }}">
                        <label class="control-label error_phone hide" for="inputError"><i class="fa fa-times-circle-o"></i> <span class="error_phone_message"></span></label>
                    </div>
                    <div class="password_wrapper form-group">
                        <label for="mobilePhone">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                        <label class="control-label error_password hide" for="inputError"><i class="fa fa-times-circle-o"></i> <span class="error_password_message"></span></label>
                    </div>
                    <div class="password_confirm_wrapper form-group">
                        <label for="mobilePhone">Confirm Password</label>
                        <input type="password" class="form-control" id="password_confirm" name="password_confirm">
                        <label class="control-label error_confirm_password hide" for="inputError"><i class="fa fa-times-circle-o"></i> <span class="error_confirm_password_message"></span></label>
                    </div>
                    <div class="form-group">
                        <label for="role">Role</label>
                        <select name="role" class="form-control" id="role">
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}" @if($user->role == $role->id){{'selected'}}@endif>{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <input type="hidden" name="user_id" value="{{ $user->id }}" class="user_id" />
                    <button type="submit" class="btn btn-flat btn-primary">Submit</button>
                    <a href="{{ URL::route('user.index') }}" type="button" class="btn btn-flat btn-default">Cancel</a>
                </div>
            </form>
        </div><!-- /.box -->
    </div><!--/.col (right) -->
</div>   <!-- /.row -->
@stop

@section('scripts')
    <script src="{{ asset('assets/backend/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
     <script type="text/javascript" src="{{asset('assets/backend/js/modules/user.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/backend/js/modules/errors/user_errors.js')}}"></script>
@stop
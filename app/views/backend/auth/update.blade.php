<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Update Password</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.4 -->
        <link href="{{asset('assets/backend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{asset('assets/backend/css/app.css')}}" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="{{asset('assets/backend/plugins/iCheck/square/blue.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('assets/backend/plugins/waitme/waitMe.css') }}" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

        <script type="text/javascript">
            var globalData = {
                'baseUrl': "{{ URL::to('/') }}"
            };
        </script>
    </head>
    <body class="login-page">
        <div class="page-loader"></div>
        <div class="login-box">
            <div class="login-logo">
            <b>{{Config::get('general_settings.application_name')}}</b>{{Config::get('general_settings.site_name')}}
            </div><!-- /.login-logo -->

            <div class="login-box-body forgotPasswordBox">
                <p class="login-box-msg">Please update your password</p>
                <form action="" method="post" id="update-password">
                    <div class="form-group has-feedback">
                        <input type="password" name="password" id="password" class="form-control" placeholder="New Password"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm Password"/>
                    </div>

                    <div class="row">
                        <!--
                        <div class="col-xs-4">
                            <button type="button" class="btn btn-default btn-block btn-flat">Back</button>
                        </div>
                        -->
                        <div class="col-xs-8 pull-right">
                            <input type="hidden" name="user_id" id="user_id" class="form-control" value="{{ $user_id }}" />
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Update Password</button>
                        </div><!-- /.col -->
                    </div>
                </form>
            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="{{asset('assets/backend/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{asset('assets/backend/js/bootstrap.min.js')}}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{asset('assets/backend/plugins/iCheck/icheck.min.js')}}" type="text/javascript"></script>
<!-- Load Modules JS -->

<script src="{{ asset('assets/backend/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/backend/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/backend/js/functions.js') }}" type="text/javascript"></script>

<script src="{{asset('assets/backend/js/modules/update_password.js')}}" type="text/javascript"></script>

</body>
</html>
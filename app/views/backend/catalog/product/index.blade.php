@extends('layouts.backend')
@section('content')
@include('backend.modal.product_image')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Product</h3>
                <h2><a href="{{URL::to('admin/product/create')}}" class="btn btn-primary btn-flat">Create New</a></h2>
            </div><!-- /.box-header -->
        <div class="box-body">
            <div class="productTable">
                {{ $tables->render() }}
            </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript" src="{{asset('assets/backend/js/modules/product.js')}}"></script>
@stop

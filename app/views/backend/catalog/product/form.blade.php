@extends('layouts.backend')
@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
                <div class="alert alert-danger alert-dismissible hide errorMessages" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Error!</strong> <span class="messages"></span>
                </div>
            </div><!-- /.box-header -->
                <!-- form start -->
            <form role="form" id="" class="generalSubmitForm" id="productForm" action="{{URL::to('admin/product/save')}}">
                <div class="box-body">
                    <div class="col-md-11">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_general" data-toggle="tab">General</a></li>
                   <li><a href="#tab_data" data-toggle="tab">Data</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_general">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="@if(isset($product)) {{ $product->name }} @endif">
                        <input type="hidden" class="form-control" id="id" name="id" value="@if(isset($product)) {{ $product->id }} @endif">
                        <input type="hidden" class="form-control" id="action" name="action" value="@if(isset($product)) edit @else add @endif">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Product Category</label>
                        <select name="category" id="category" class="form-control">
                            <option value="">- Choose Category -</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description</label>
                        <textarea class="form-control" id="description" name="description">@if(isset($product)) {{ $product->description }} @endif</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Highlight</label>
                        <textarea class="form-control" id="highlight" name="highlight">@if(isset($product)) {{ $product->highlight }} @endif</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Size Details</label>
                        <textarea class="form-control" id="size_details" name="size_details">@if(isset($product)) {{ $product->size_details }} @endif</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Return Policy</label>
                        <textarea class="form-control" id="return_policy" name="return_policy">@if(isset($product)) {{ $product->return_policy }} @endif</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Delivery Info</label>
                        <textarea class="form-control" id="delivery_info" name="delivery_info">@if(isset($product)) {{ $product->delivery_info }} @endif</textarea>
                    </div>
                    <div class="form-group">
                        <label>Featured Image </label>
                        <div class="input-group col-md-3">
                            <input type="hidden" name="image" id="image" value="">
                            <a href="javascript:void(0)" class="thumbnail link-uploader" onclick="" id="uploadLink" url="{{URL::to('admin/upload/image/product')}}">
                                <img src="" class="img-uploader">
                            </a>
                        </div>
                    </div>
                    </div><!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_data">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Price</label>
                            <input type="text" class="form-control" id="price" name="price" value="@if(isset($product)) {{ $product->price }} @endif">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Weight</label>
                            <input type="text" class="form-control" id="weight" name="weight" value="@if(isset($product)) {{ $product->weight }} @endif">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Quantity</label>
                            <input type="text" class="form-control" id="quantity" name="quantity" value="@if(isset($product)) {{ $product->quantity }} @endif">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Discount</label>
                            <input type="text" class="form-control" id="discount" name="discount" value="@if(isset($product)) {{ $product->discount }} @endif">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">- Choose Status -</option>
                                <option value="1"> In Stock </option>
                                <option value="2"> Pre Order  </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Size</label>
                            <select name="size" id="size" class="form-control">
                                <option value="">- Choose Size -</option>
                                <option value="S"> S </option>
                                <option value="M"> M </option>
                                <option value="L"> L </option>
                                <option value="XL"> XL </option>
                                <option value="XXL"> XXL </option>
                                <option value="XXXL"> XXXL </option>
                            </select>
                        </div>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Submit</button>
                    <a href="{{URL::to('admin/product')}}"><button type="button" class="btn btn-flat btn-default">Back</button></a>
                </div>
            </form>
        </div><!-- /.box -->
    </div><!--/.col (right) -->
</div>   <!-- /.row -->
<script type="text/javascript" src="{{asset('assets/backend/js/modules/upload.js')}}"></script>
@stop
@extends('layouts.backend')
@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
                <div class="alert alert-danger alert-dismissible hide errorMessages" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Error!</strong> <span class="messages"></span>
                </div>
            </div><!-- /.box-header -->
                <!-- form start -->
            <form role="form" id="" class="generalSubmitForm" id="categoryForm" action="{{URL::to('admin/category/save')}}">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="@if(isset($category)) {{ $category->name }} @endif">
                        <input type="hidden" class="form-control" id="id" name="id" value="@if(isset($category)) {{ $category->id }} @endif">
                        <input type="hidden" class="form-control" id="action" name="action" value="@if(isset($category)) edit @else add @endif">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Parent Category</label>
                        <select name="parent_id" id="parent_id" class="form-control">
                            <option value="">- Choose Category -</option>
                            @foreach($parents as $parent)
                                <option option="{{$parent['id']}}">{{$parent['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Sort Order</label>
                        <input type="text" class="form-control" id="sort_order" name="sort_order" value="@if(isset($category)) {{ $category->sort_order }} @endif">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description</label>
                        <textarea class="form-control" id="description" name="description">@if(isset($category)) {{ $category->description }} @endif</textarea>
                    </div>
                    <div class="form-group">
                        <label>Featured Image </label>
                        <div class="input-group col-md-3">
                            <input type="hidden" name="image" id="image" value="">
                            <a href="javascript:void(0)" class="thumbnail link-uploader" onclick="" id="uploadLink" url="{{URL::to('admin/upload/image/category')}}">
                                <img src="" class="img-uploader">
                            </a>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Submit</button>
                    <a href="{{URL::to('admin/category')}}"><button type="button" class="btn btn-flat btn-default">Back</button></a>
                </div>
            </form>
        </div><!-- /.box -->
    </div><!--/.col (right) -->
</div>   <!-- /.row -->
<script type="text/javascript" src="{{asset('assets/backend/js/modules/upload.js')}}"></script>
@stop
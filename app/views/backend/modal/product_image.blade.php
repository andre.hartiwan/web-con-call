<div class="modal" id="productImageModal">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title"><buttton class="btn btn-flat btn-primary"><i class="fa fa-plus"></i> Add New</buttton></h4>
          </div>
          <div class="modal-body">
            <div class="col-md-12 imageList">

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-flat btn-warning" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
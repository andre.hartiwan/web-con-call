<!DOCTYPE html>

<html lang="en" class="no-js">

<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Batik Warisan</title>
    <!-- Global styles START -->
    <link href="{{asset('assets/frontend/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/frontend/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/frontend/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/frontend/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- Page level plugin styles START -->
    <link href="{{asset('assets/frontend/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/plugins/bxslider/jquery.bxslider.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/frontend/plugins/layerslider/css/layerslider.css')}}" type="text/css">
    <!-- Page level plugin styles END -->

    <!-- BEGIN THEME STYLES -->
    <link href="{{asset('assets/frontend/css/style-metronic.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/frontend/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/frontend/css/style-responsive.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/frontend/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/frontend/css/custom.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME STYLES -->
</head>
<!-- END HEAD -->
<script type="text/javascript">
    var globalData = {
        'baseUrl': "{{ URL::to('/') }}"
    };
</script>
<!-- BEGIN BODY -->
<body >
    <div class="outer-body">
            <!--BEGIN SIDEBAR-->
            <div style="width:300px; position:absolute !important;z-index:9999;" id="sidebar">
                <a href="#DASHBOARD" data-toggle="tab">
                    <img src="{{asset('assets/frontend/images/logo.png')}}" class="col-12" height="198" />
                </a>
                <div class="col-12" style="height:640px;background-image:url({{asset('assets/frontend/images/sidebar.png')}});background-repeat:no-repeat; ">
                    <div class="col-12">
                        <a href ="dashboard.html"  class="padding-low" style="text-decoration:underline;color:white">
                            <h2 style="padding-left:20px;">HOME &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h2>
                        </a>
                    </div>
                    <div class="col-12">
                        <a href="newarrival.html"  class="padding-low" style="text-decoration:underline;color:white">
                            <h2 style="padding-left:20px;">NEW ARRIVALS</h2>
                        </a>
                    </div>
                    <div class="col-12">
                        <a href="magazine.html" data-toggle="tab" class="padding-low" style="text-decoration:underline;color:white">
                            <h2 style="padding-left:20px;">MAGAZINE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h2>
                        </a>
                    </div>
                    <div class="col-12">
                        <a href="about.html" data-toggle="tab" class="padding-low" style="text-decoration:underline;color:white">
                            <h2 style="padding-left:20px;">ABOUT &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h2>
                        </a>
                    </div>
                    <div class="col-12">
                        <a href="testimonial.html" data-toggle="tab" class="padding-low" style="text-decoration:underline;color:white">
                            <h2 style="padding-left:20px;">TESTIMONIALS</h2>
                        </a>
                    </div>
                </div>
            </div>
            <!--END SIDEBAR-->
            <!--BEGIN CONTENT-->
            <div class="col-12" >
                <header>
                    <div class="col-12" style="height:117.6px;"></div>
                    <!--BEGIN UPPER MENU-->
                    <div class="col-12" style="height:40.2px;border-bottom:4px solid #58260c;">
                        <table class="col-4 pull-right">
                            <tr>
                                <td class="padding-med col-4 text-center">
                                    <img src="{{asset('assets/frontend/images/wishlist.png')}}" class="pull-left   circle" width="30" height="30"/>
                                    <div class="col-md-6 padding-med"><a href="wishlist.html" >WISHLIST</a></div>
                                </td>
                                <td class="padding-med col-4 text-center">
                                    <img src="{{asset('assets/frontend/images/account.png')}}" class="pull-left   circle" width="30" height="30" />
                                    <div class="col-md-6 padding-med"><a href="account.html">ACCOUNT</a></div>
                                </td>
                                <td class="padding-med col-4 text-center">
                                    <img src="{{asset('assets/frontend/images/bag.png')}}" class="pull-left   circle" width="30" height="30" />
                                    <div class="col-md-6 padding-med"><a href="bag.html">BAG</a></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--END UPPER MENU-->
                    <!--BEGIN NAVIGATION-->
                    <div class="col-12" style="height:40.2px;">
                        <ul class="nav nav-tabs pull-right col-8" style="margin:0px !important;border:none !important;">

                            <li class="col-2 text-center ">
                                <a href="{{URL::to('category/category_name')}}">
                                    TOPS
                                </a>
                            </li>
                            <li class="col-2 text-center">
                                <a href="{{URL::to('category/category_name')}}">
                                    BOTTOMS
                                </a>
                            </li>
                            <li class="col-2 text-center">
                                <a href="{{URL::to('category/category_name')}}" >
                                    OUTERS
                                </a>
                            </li>
                            <li class="col-2 text-center">
                                <a href="{{URL::to('category/category_name')}}">
                                    DRESSES
                                </a>
                            </li>
                            <li class="col-4 text-center padding-high">
                                <form>
                                    <input class="pull-left col-10 text-center" type="text"/>
                                    <button type="submit" style="margin-top:-2px !important;background-color:brown !important;" class="padding-low pull-left"><i class="fa fa-search" style="color:white !important;"></i></button>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <!--END NAVIGATION-->
                </header>
                <!--BEGIN PAGE-->
                <section class="content">
                @yield('content')
                </section><!-- /.content -->
            </div>
            <!--END CONTENT-->
            <!--BEGIN FOOTER-->
            <div class="col-12 " style="height:200px;">
                <table class="col-12">
                    <tr>
                        <td class="col-3" style="height:200px">
                            <a href="#sidebar">
                                <img src="{{asset('assets/frontend/images/logo.png')}}" class="col-12" height="200" />
                            </a>
                        </td>
                        <td class="col-1"></td>
                        <td class="col-8" style="height:150px">
                            <table class="col-12">
                                <tr>
                                    <td class="col-2  " style="vertical-align:top; height:150px;border-left:2px solid #58260c;padding-left:10px;padding-right:10px;"">
                                        <h4>Service</h4><br />
                                        <a href="#">FAQ</a><br />
                                        <a href="#">Payment Confirmation</a><br />
                                        <a href="#">Contact Us</a><br />
                                        <a href="#">Shipping Information</a><br />
                                    </td>
                                    <td class="col-2  " style="vertical-align:top; height:150px;border-left:2px solid #58260c;padding-left:10px;padding-right:10px;"">
                                        <h4>About Batik Warisan</h4><br />
                                        <a href="#">About Us</a><br />
                                        <a href="#">Carrier</a><br />
                                        <a href="#">Term & Condition</a><br />
                                    </td>
                                    <td class="col-2  " style="vertical-align:top; height:150px;border-left:2px solid #58260c;padding-left:10px;padding-right:10px;">
                                        <h4>Follow Us</h4><br />
                                        <a href="#">Facebook</a><br />
                                        <a href="#">Twitter</a><br />
                                        <a href="#">Instagram</a><br />
                                        <a href="#">Path</a><br />
                                        <a href="#">Google+</a><br />
                                    </td>
                                    <td class="col-2  " style="vertical-align:top; height:150px;border-left:2px solid #58260c;padding-left:10px;padding-right:10px;">
                                        <h4>Payments</h4><br />
                                        <a href="#"><img src="" width="120" height="30" class="bordered" /> </a><br />
                                        <a href="#"><img src="" width="120" height="30" class="bordered"/> </a><br />
                                        <a href="#"><img src="" width="120" height="30" class="bordered" /> </a><br />
                                    </td>
                                    <td class="col-2  " style="vertical-align:top; height:150px;border-left:2px solid #58260c;padding-left:10px;padding-right:10px;">
                                        <h4>Delivery Service</h4><br />
                                        <a href="#"><img src="{{asset('assets/frontend/images/jne.png')}}" width="120" height="80" /> </a><br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <!--END FOOTER-->
    </div>
    <script src="{{asset('assets/frontend/plugins/jquery-1.10.2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend/plugins/jquery-migrate-1.2.1.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend//plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/fancybox/source/jquery.fancybox.pack.js')}}"></script><!-- pop up -->
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/bxslider/jquery.bxslider.min.js')}}"></script><!-- slider for products -->
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/zoom/jquery.zoom.min.js')}}"></script><!-- product zoom -->
    <script src="{{asset('assets/frontend/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->
    <!-- BEGIN LayerSlider -->
    <script src="{{asset('assets/frontend/plugins/layerslider/jQuery/jquery-easing-1.3.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend/plugins/layerslider/jQuery/jquery-transit-modified.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend/plugins/layerslider/js/layerslider.transitions.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/frontend/plugins/layerslider/js/layerslider.kreaturamedia.jquery.js')}}" type="text/javascript"></script>
    <!-- END LayerSlider -->
    <script type="text/javascript" src="{{asset('assets/frontend/plugins/back-to-top.js')}}"></script>
    <script src="{{asset('assets/frontend/plugins/rateit/src/jquery.rateit.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/js/app.js')}}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            App.initImageZoom();
        });
    </script>

</body>
<!-- END BODY -->
</html>
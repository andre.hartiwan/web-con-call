<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}


	public function getDashboard()
	{
		return View::make('backend.dashboard');
	}

	public function getIndex()
	{
		return View::make('frontend.index');
	}

	public function getCategory($category)
	{
		return View::make('frontend.category');
	}

}

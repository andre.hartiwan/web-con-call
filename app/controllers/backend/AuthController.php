<?php

    class AuthController extends BaseBackendController 
    {
        public function getSignin()
        {
            if(Sentry::check()) return Redirect::route('dashboard.index');

            return View::make('backend.auth.signin');
        }

        public function postSignin()
        {
            $form_processor = new AuthSigninRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new AuthSigninProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('update_password', $processor->getOutput()), 200);
            }
        }

        public function postForgotPassword()
        {
            echo '<pre>';print_r('$variable');
            die();
        }

        public function getSignout()
        {
            Sentry::logout();
            return Redirect::route('auth.signin');
        }

        public function postUpdatePassword()
        {
            $form_processor = new AuthUpdatePwdRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new AuthUpdatePwdProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('update_password', $processor->getOutput()), 200);
            }
        }
    }

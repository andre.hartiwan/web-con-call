<?php

	class DashboardController extends BaseBackendController 
	{
		public function getIndex()
		{
			return View::make('backend.dashboard');
		}
	}

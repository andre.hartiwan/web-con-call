<?php

class CategoryController extends BaseBackendController {

    public function getIndex()
    {
        $tables = Datatable::table()
                ->addColumn('Image', 'Parent ID', 'Description', 'Actions')
                ->setUrl(route('admin.api.categories'))
                ->noScript();
        return View::make('backend.catalog.category.index', array('tables' => $tables));
    }

    public function getCreate()
    {
        $parents = Category::where('parent_id', 0)->get();
        return View::make('backend.catalog.category.form', array('parents' => $parents));
    }


    public function postSave()
    {
        //validate
        $action = trim(Input::get('action'));
        $id = Input::get('id');
        //set rules
        $rules = array(
            'name' => 'required'
            );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $error = 1;
            $message = $validator->messages();
        } else {
            $id = Input::get('id');
            if ($action == 'add') {
                $category = new Category;
            } else {
                $category = Category::find($id);
            }
            $category->name = Input::get('name');
            $category->parent_id = Input::get('parent_id');
            $category->sort_order = Input::get('sort_order');
            $category->image = trim(Input::get('image'));
            $category->description = Input::get('description');
            if($category->save()) {
                $error = 0;
                $messages = array('messages' => 'Category saved !');
            } else {
                $error = 1;
                $messages = array('messages' => 'Save failed !');
            }

            $response['error'] = $error;
            $response['messages'] = $messages;
            $response['redirect'] = URL::to('admin/category');
            echo json_encode($response);
            exit();
        }
    }


    public function getDataTables()
    {
        $categories = New Category;
        $query = $categories->select('id', 'name', 'parent_id', 'sort_order', 'image', 'description')->get();
        $data = Datatable::collection($query);
        return $data
            ->addColumn('name', function($model) {
                return $model->name;
            })
            ->addColumn('parent_id', function($model) {
                return $model->parent_id;
            })
            ->addColumn('description', function($model) {
                return $model->description;
            })
            ->addColumn('action', function($model) {
                $action  = '';
                $action .= '<a href="'.URL::to("admin/category/edit/".$model->id).'" class="btn btn-success btn-flat" title="Edit category" content-id ="'.$model->id.'" message=""><i class="fa fa-pencil"></i></a>';
                // $action .= '<a href="javascript:void(0)" class="btn btn-primary btn-flat" title="View category" content-id ="'.$model->id.'" message="" onclick="getDetail(this)"><i class="fa fa-search-plus"></i></a>';
                $action .=  '<a href="javascript:void(0)" class="btn btn-danger btn-flat btn-delete" title="Delete category" content-id ="'.$model->id.'" onclick="confirmModal(this)" message="Are you sure delete this category ?" type="category"><i class="fa fa-eraser"></i></a>';
                return $action;
            })
            ->searchColumns('name', 'sort_order')
            ->orderColumns('name', 'sort_order')
            ->make();
    }

    public function getEdit($id)
    {
        $category = Category::find($id);
        if($category) {
            $parents = Category::where('parent_id', 0)->get();
            return View::make('backend.catalog.category.form', array('category' => $category, 'parents' => $parents));
        } else {

        }
    }

    public function postDelete()
    {
        /*TODO*/
        /*Validation to check deleted category is parent category*/
        $id = Input::get('id');
        $category = Category::find($id);
        if($category) {
            if(Category::hasChild($id)) {
                $error = 1;
            } else {
                if($category->delete()) {
                    $error = 0;
                } else {
                    $error = 1;
                }
            }
        } else {
            $error = 1;
        }

        if($error)
            $message = 'Failed to delete category';
        else
            $message = '';

        $response['error'] = $error;
        $response['message'] = $message;
        $response['redirect'] = URL::to('admin/category');
        echo json_encode($response);
        exit();

    }

}

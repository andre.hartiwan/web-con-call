<?php

	class BaseBackendController extends Controller 
	{
		public function __construct()
		{
			if(Sentry::check())
			{
				$credential = Sentry::getUser();

				$the_user = User::find($credential->id);

				View::share('the_user', $the_user);
				View::share('credential', $credential);	
			}
		}

		protected function setupLayout()
		{
			if ( ! is_null($this->layout))
			{
				$this->layout = View::make($this->layout);
			}
		}

	}

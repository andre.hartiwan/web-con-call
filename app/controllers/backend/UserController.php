<?php
    
    class UserController extends BaseBackendController 
    {
        public function getIndex()
        {
            $tables = Datatable::table()
                    ->addColumn('First Name', 'Last Name', 'Email', 'Role', 'Last Login', 'Status', 'Actions')
                    ->setUrl(route('admin.api.users'))
                    ->noScript();

            return View::make('backend.user.index', array('tables' => $tables));
        }

        public function  getCreate()
        {
            $data['roles'] = Role::all();
            return View::make('backend.user.add', $data);
        }

        public function postCreate()
        {
            $form_processor = new UserAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new UserAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

        public function getUserDataTables()
        {
            return UserManager::generateDataTables();
        }

        public function getEdit()
        {
            $user_id = Input::get('id');
            $data['roles'] = Role::all();
            $data['user'] = UserManager::get($user_id);
            return View::make('backend.user.edit', $data);
        }

        public function postEdit()
        {
            $form_processor = new UserEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new UserEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

        public function postDelete()
        {
            $form_processor = new UserDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new UserDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }
    }

<?php

class ProductController extends BaseBackendController {

    public function getIndex()
    {

        $tables = Datatable::table()
                ->addColumn('Name', 'Description', 'Price', 'Quantity', 'Discount', 'Actions')
                ->setUrl(route('admin.api.products'))
                ->noScript();
        return View::make('backend.catalog.product.index', array('tables' => $tables));
    }

    public function getCreate()
    {
        $categories = Category::all();
        return View::make('backend.catalog.product.form', array('categories' => $categories));
    }


    public function postSave()
    {
        //validate
        $action = trim(Input::get('action'));
        $id = Input::get('id');
        //set rules
        $rules = array(
            'name' => 'required',
            'price' => 'required',
            'weight' => 'required',
            'quantity' => 'required|integer',
            'size' => 'required',
            'description' => 'required',
            'highlight' => 'required',
            'size_details' => 'required',
            'return_policy' => 'required',
            'delivery_info' => 'required',
            'status' => 'required',
            // 'image' => 'required',
            'category' => 'required|integer'
            );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $error = 1;
            $messages = $validator->messages();
            $response['error'] = $error;
            $response['messages'] = $messages;
            echo json_encode($response);
            exit();
        } else {
            $id = Input::get('id');
            if ($action == 'add') {
                $product = new Product;
            } else {
                $product = Product::find($id);
            }

            $product->name = Input::get('name');
            $product->description = Input::get('description');
            $product->highlight = Input::get('highlight');
            $product->image = Input::get('image');
            $product->status = Input::get('status');
            $product->quantity = Input::get('quantity');
            $product->weight = Input::get('weight');
            $product->price = Input::get('price');
            $product->discount = Input::get('discount');
            $product->size = Input::get('size');
            $product->size_details = Input::get('size_details');
            $product->return_policy = Input::get('return_policy');
            $product->delivery_info = Input::get('delivery_info');
            $product->category_id = Input::get('category');

            if($product->save()) {
                $error = 0;
                $messages = array('messages' => 'Product saved !');
            } else {
                $error = 1;
                $messages = array('messages' => 'Save failed !');
            }
            $response['error'] = $error;
            $response['messages'] = $messages;
            $response['redirect'] = URL::to('admin/product');
            echo json_encode($response);
            exit();
        }
    }


    public function getDataTables()
    {
        $products = New Product;
        $query = $products->select('id', 'name', 'description', 'quantity', 'image', 'price', 'discount')->get();
        $data = Datatable::collection($query);
        return $data
            ->addColumn('name', function($model) {
                return $model->name;
            })
            ->addColumn('description', function($model) {
                return $model->description;
            })
            ->addColumn('price', function($model) {
                return $model->price;
            })
            ->addColumn('quantity', function($model) {
                return $model->quantity;
            })
            ->addColumn('discount', function($model) {
                return $model->discount;
            })
            ->addColumn('action', function($model) {
                $action  = '';
                $action .= '<a href="'.URL::to("admin/product/edit/".$model->id).'" class="btn btn-success btn-flat" title="Edit product" content-id ="'.$model->id.'" message=""><i class="fa fa-pencil"></i></a>';
                $action .= '<a href="javascript:void(0)" class="btn btn-primary btn-flat" title="Image" content-id ="'.$model->id.'" message="" onclick="getProductImage(this)"><i class="fa fa-image"></i></a>';
                $action .=  '<a href="javascript:void(0)" class="btn btn-danger btn-flat btn-delete" title="Delete product" content-id ="'.$model->id.'" onclick="confirmModal(this)" message="Are you sure delete this product ?" type="product"><i class="fa fa-eraser"></i></a>';
                return $action;
            })
            ->searchColumns('name', 'description')
            ->orderColumns('name', 'description')
            ->make();
    }

    public function getEdit($id)
    {
        $product = Product::find($id);
        if($product) {
            $categories = Category::all();
            return View::make('backend.catalog.product.form', array('product' => $product, 'categories' => $categories));
        } else {

        }
    }

    public function postDelete()
    {
        /*TODO*/
        /*Validation to check deleted product is parent product*/
        $id = Input::get('id');
        $product = Product::find($id);
        if($product) {
            if(Product::hasChild($id)) {
                $error = 1;
            } else {
                if($product->delete()) {
                    $error = 0;
                } else {
                    $error = 1;
                }
            }
        } else {
            $error = 1;
        }

        if($error)
            $message = 'Failed to delete product';
        else
            $message = '';

        $response['error'] = $error;
        $response['message'] = $message;
        $response['redirect'] = URL::to('admin/product');
        echo json_encode($response);
        exit();

    }

}

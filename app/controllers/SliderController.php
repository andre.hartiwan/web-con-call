<?php

class SliderController extends BaseController {

    public function getIndex()
    {
        $tables = Datatable::table()
                ->addColumn('Image', 'Title', 'Description', 'Actions')
                ->setUrl(route('admin.api.sliders'))
                ->noScript();
        return View::make('backend.slider.index', array('tables' => $tables));
    }

    public function getCreate()
    {
        return View::make('backend.slider.form');
    }


    public function postSave()
    {
        //validate
        $action = trim(Input::get('action'));
        $id = Input::get('id');
        //set rules
        $rules = array(
            'title' => 'required'
            );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $error = 1;
            $message = $validator->messages();
        } else {
            $id = Input::get('id');
            if ($action == 'add') {
                $slider = new Slider;
            } else {
                $slider = Slider::find($id);
            }
            $slider->title = Input::get('title');
            $slider->image = trim(Input::get('image'));
            $slider->sort_order = Input::get('sort_order');
            $slider->description = Input::get('description');
            if($slider->save()) {
                $error = 0;
                $messages = array('messages' => 'Slider saved !');
            } else {
                $error = 1;
                $messages = array('messages' => 'Save failed !');
            }

            $response['error'] = $error;
            $response['messages'] = $messages;
            $response['redirect'] = URL::to('admin/slider');
            echo json_encode($response);
            exit();
        }
    }


    public function getDataTables()
    {
        $sliders = New Slider;
        $query = $sliders->select('id', 'title', 'sort_order', 'image', 'description')->get();
        $data = Datatable::collection($query);
        return $data
            ->addColumn('image', function($model) {
                $imagePath = Config::get('image_path.slider').DIRECTORY_SEPARATOR.$model->image;
                if(file_exists($imagePath))
                    $thumbnail = $model->image;
                else
                    $thumbnail = 'default.png';

                return '<a href="javascript:void(0)" class="thumbnail"><img src="'.asset('assets/uploads/images/slider').'/'.$thumbnail.'" class="table-thumbnail"></a>';
            })
            ->addColumn('title', function($model) {
                return $model->title;
            })
            ->addColumn('description', function($model) {
                return $model->description;
            })
            ->addColumn('action', function($model) {
                $action  = '';
                $action .= '<a href="'.URL::to("admin/slider/edit/".$model->id).'" class="btn btn-success btn-flat" title="Edit slider" content-id ="'.$model->id.'" message=""><i class="fa fa-pencil"></i></a>';
                // $action .= '<a href="javascript:void(0)" class="btn btn-primary btn-flat" title="View slider" content-id ="'.$model->id.'" message="" onclick="getDetail(this)"><i class="fa fa-search-plus"></i></a>';
                $action .=  '<a href="javascript:void(0)" class="btn btn-danger btn-flat btn-delete" title="Delete slider" content-id ="'.$model->id.'" onclick="confirmModal(this)" message="Are you sure delete this slider ?" type="slider"><i class="fa fa-eraser"></i></a>';
                return $action;
            })
            ->searchColumns('title', 'sort_order')
            ->orderColumns('title', 'sort_order')
            ->make();
    }

    public function getEdit($id)
    {
        $slider = Slider::find($id);
        if($slider) {
            return View::make('backend.slider.form', array('slider' => $slider));
        } else {

        }
    }

    public function postDelete()
    {
        $id = Input::get('id');
        $slider = Slider::find($id);
        if($slider) {
            if($slider->delete()) {
                $error = 0;
            } else {
                $error = 1;
            }
        } else {
            $error = 1;
        }

        if($error)
            $message = 'Failed to delete slider';
        else
            $message = '';

        $response['error'] = $error;
        $response['message'] = $message;
        $response['redirect'] = URL::to('admin/slider');
        echo json_encode($response);
        exit();

    }

}

function showUserError(data)
{
    if(data.data.errors.first_name != undefined)
    {
        $('.first_name_wrapper').addClass("has-error");
        $('.error_first_name').removeClass('hide');
        $('.error_first_name_message').html(data.data.errors.first_name);
    }
    else
    {
        $('.first_name_wrapper').removeClass("has-error");
        $('.error_first_name').addClass('hide');
        $('.error_first_name_message').empty();
    }

    if(data.data.errors.email != undefined)
    {
        $('.email_wrapper').addClass("has-error");
        $('.error_email').removeClass('hide');
        $('.error_email_message').html(data.data.errors.email);
    }
    else
    {
        $('.email_wrapper').removeClass("has-error");
        $('.error_email').addClass('hide');
        $('.error_email_message').empty();
    }

    if(data.data.errors.mobile_phone != undefined)
    {
        $('.phone_wrapper').addClass("has-error");
        $('.error_phone').removeClass('hide');
        $('.error_phone_message').html(data.data.errors.mobile_phone);
    }
    else
    {
        $('.phone_wrapper').removeClass("has-error");
        $('.error_phone').addClass('hide');
        $('.error_phone_message').empty();
    }

    if(data.data.errors.password != undefined)
    {
        $('.password_wrapper').addClass("has-error");
        $('.error_password').removeClass('hide');
        $('.error_password_message').html(data.data.errors.password);
    }
    else
    {
        $('.password_wrapper').removeClass("has-error");
        $('.error_password').addClass('hide');
        $('.error_password_message').empty();
    }

    if(data.data.errors.confirm_password != undefined)
    {
        $('.password_confirm_wrapper').addClass("has-error");
        $('.error_confirm_password').removeClass('hide');
        $('.error_confirm_password_message').html(data.data.errors.confirm_password);
    }
    else
    {
        $('.confirm_password_wrapper').removeClass("has-error");
        $('.error_confirm_password').addClass('hide');
        $('.error_confirm_password_message').empty();
    }
}
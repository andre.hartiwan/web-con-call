$(document).ready(function() {
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
    });

    /*trigger for submit signin form*/
    $('#signinForm').submit(function(e) {

        e.preventDefault();

        $('.page-loader').fadeIn();

        $.ajax({
            type: 'POST',
            url: globalData.baseUrl + '/auth/signin',
            dataType: 'json',
            data: $(this).serialize(),
            success: function(data) {
                $('.page-loader').fadeOut();

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    window.location.replace(globalData.baseUrl + '/admin/dashboard');
                }
            }
        });
    });

    /*trigger for submit signin form*/
    $('#forgotPasswordForm').submit(function() {
        $('.page-loader').fadeIn();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            dataType: 'json',
            data: $(this).serialize(),
            success: function(data) {
                $('.page-loader').fadeOut();
                if (data.error === 0) {
                    window.location.href = data.redirect;
                } else {
                    $('#errorMessage').show().removeClass('hide');
                    messages ='';
                    $.each(data.messages, function(index, value) {
                        messages += '<p>' + value + '</p>';
                    });
                    $('#errorMessage').html(error_message);
                    $('#errorMessage').fadeIn();

                }

            }
        });
        return false;
    });

    /*trigger for button forgot password*/
    $('.btnForgotPassword').click(function() {
        $('.page-loader').fadeIn();
        $('.forgotPasswordBox').removeClass('hide');
        $('.signinBox').addClass('hide');
        $('.page-loader').fadeOut();
    });

    /*trigger for button back to login form*/
    $('.btnBackToSignin').click(function() {
        $('.page-loader').fadeIn();
        $('.forgotPasswordBox').addClass('hide');
        $('.signinBox').removeClass('hide');
        $('.page-loader').fadeOut();
    });
});
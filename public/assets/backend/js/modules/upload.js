    //general function to upload image
    $(function() {
        var image = $('#image');
        var urlDestination = $('.link-uploader').attr('url');
        var uploader = $('.link-uploader');
        new AjaxUpload(uploader, {
            action: urlDestination,
            name: 'image',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                    $('.loader-page').fadeOut();
                    alert('Only JPG, PNG, or GIF files are allowed');
                    return false;
                }
            }, onComplete: function(file, response) {
                //On completion clear the status
                var json = $.parseJSON(response);
                //Add uploaded file to list
                if (json.error === 0) {
                    console.log(json);
                    $('.img-uploader').attr('src', json.image_path);
                    $('#image').val(json.filename);
                } else {
                    alert(json.message);
                }
            }
        });
    });
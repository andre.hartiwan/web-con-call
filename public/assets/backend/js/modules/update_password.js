$(document).ready(function() {

    $('#update-password').submit(function(e) {

        e.preventDefault();
        
        var form_data = new FormData(this);
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: globalData.baseUrl + '/auth/update-password',
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.login-box-body').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.login-box-body').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    window.location.replace(globalData.baseUrl + '/admin/dashboard');
                }

            }
        });
    });
});

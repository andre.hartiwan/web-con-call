$(document).ready(function() {
    $('.generalSubmitForm').submit(function() {
        // $('.page-loader').fadeIn();
        var urlDestination = $(this).attr('action');
        var formData = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: urlDestination,
            dataType: 'json',
            data: formData,
            success: function(data) {
                $('.page-loader').fadeOut();
                if (data.error === 0) {
                    window.location.href = data.redirect;
                } else {
                    $('.errorMessages').removeClass('hide');
                    errors = '';
                    $.each(data.messages, function(index, value) {
                        errors += '<p>' + value + '</p>';
                    });
                    $('.errorMessages').show().html(errors);
                }
            }
        });
        return false;
    });

    //set general uploader
    $('.img-uploader').attr('src', globalData.baseUrl+'/assets/backend/img/upload.png');
    //create action for delete button
    $('.yes-delete').click(function() {
        $('.page-loader').fadeIn();
        var id = $(this).attr('data-id');
        var type = $(this).attr('type');
        var urlDestination = globalData.baseUrl+'/admin/'+type+'/delete';
        $.ajax({
            type: 'POST',
            url: urlDestination,
            dataType: 'json',
            data: {id : id},
            success: function(data) {
                $('.page-loader').fadeOut();
                if (data.error === 0) {
                    window.location.href = data.redirect;
                } else {
                    $('#alertModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            }
        });
    });
});


function confirmModal(elements) {
    var title = $(elements).attr('title');
    var message = $(elements).attr('message');
    var id = $(elements).attr('content-id');
    var type  = $(elements).attr('type');
    $('#confirmModal h4.modal-title').html(title);
    $('#confirmModal .modal-body p').html(message);
    $('#confirmModal .yes-delete').attr('data-id', id);
    $('#confirmModal .yes-delete').attr('type', type);
    $('#confirmModal').modal({
        backdrop: 'static',
        keyboard: false
    });
}
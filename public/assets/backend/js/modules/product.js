$(document).ready(function() {
    $.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
       //draw the 'current' page
       oSettings.oApi._fnDraw(oSettings);
   };

    var userTable = $('.productTable table').dataTable({
        "sPaginationType": "full_numbers",
        "bProcessing": false,
        "sAjaxSource": globalData.baseUrl+'/admin/api/products/',
        "bServerSide": true,
    });
});


function getProductImage(elements)
{
    //request to server to get list
    $('#productImageModal').modal({
        backdrop: 'static',
        keyboard: false
    });
}

$(document).ready(function() {
    $.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
       //draw the 'current' page
       oSettings.oApi._fnDraw(oSettings);
    };

    var userTable = $('.userTable table').dataTable({
        "sPaginationType": "full_numbers",
        "bProcessing": false,
        "sAjaxSource": globalData.baseUrl+'/admin/user/api/users',
        "bServerSide": true,
    });

    $('#add-user, #edit-user').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
            case 'add':
                var url = globalData.baseUrl + '/admin/user/create';
                var redirect_url = globalData.baseUrl + '/admin/user';
                break;

            case 'edit':
                var url = globalData.baseUrl + '/admin/user/edit';
                var redirect_url = globalData.baseUrl + '/admin/user';
                break;
        }
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.box').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.box').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    //showError(data);
                    showUserError(data);
                }
                else
                {
                    showSuccess('User is successfully saved', redirect_url);
                }

            }
        });
    });

    $('body').on('click', '.delete', function() {
        var id = $(this).data('id');
        var url = globalData.baseUrl + '/admin/user/delete';
        var redirect_url = globalData.baseUrl + '/admin/user';
        deleteData(id, url, redirect_url);
    });
});
